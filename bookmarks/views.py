from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views import generic

from .models import Bookmark
from .forms import NewBookmarkForm


class IndexView(generic.ListView):
    template_name = 'bookmarks/index.html'

    def get_queryset(self):
        """Return the last five published bookmarks."""
        return Bookmark.objects.order_by('-created_at')[:5]

class DetailView(generic.DetailView):
    model = Bookmark
    template_name = 'bookmarks/detail.html'

class CreateView(generic.edit.CreateView):
    model = Bookmark
    template_name = 'bookmarks/new.html'
    fields = ['url']
    success_url = '/'

    def form_valid(self, form):
        # form.send_email()
        return super().form_valid(form)
