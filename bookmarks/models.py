from django.db import models


class Bookmark(models.Model):
    title = models.CharField(max_length=400, blank=True)
    url = models.URLField(max_length=400)
    description = models.CharField(max_length=1000, blank=True)
    created_at = models.DateTimeField('creation date', auto_now_add=True)

    def __str__(self):
        return self.url

'''
class MediaObject(models.Model):
    bookmark = models.ForeignKey(Bookmark, on_delete=models.CASCADE)

class Tag(models.Model):
'''
