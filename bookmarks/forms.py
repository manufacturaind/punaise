from django import forms

class NewBookmarkForm(forms.Form):
    url = forms.URLField()
