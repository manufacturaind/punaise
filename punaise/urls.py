from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('bookmarks/', include('bookmarks.urls')),
    path('admin/', admin.site.urls),
]

