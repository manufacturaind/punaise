SSH_PATH=manufactura:~/apps/punaise-dev/
SHELL=/bin/bash
ACTIVATE=. `pwd`/.env/bin/activate

build:
	mkdir -p output
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/generate.py
	cp -rf assets/* output/

install:
	python3 -m venv .env
	. `pwd`/.env/bin/activate; pip install -r requirements.txt

serve:
	. `pwd`/.env/bin/activate; cd output && python -m http.server

deploy: build
	rsync --compress --checksum --progress --recursive --update output/ $(SSH_PATH) --exclude="__pycache__" --exclude=".*"
dry-deploy: build
	rsync --compress --checksum --progress --recursive --update output/ $(SSH_PATH) --exclude="__pycache__" --exclude=".*" --dry-run
